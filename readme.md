# 3D printable intracellular electrode holder

Print with at max 0.1 mm layer height, >=3 shells, >=10% infill.

To be fixed to Amphenol 031-315-RFX BNC connector for use with Dagan intracellular amplifier headstage.

Designed to hold 1 mm OD capillary.

Finishing with M6 tap, M6 nut, thin circular file and sandpaper necessary before use.

Assembly tested.

## Prerequisits:

 - [threadlib](https://github.com/adrianschlatter/threadlib)
